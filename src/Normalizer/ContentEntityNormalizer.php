<?php

namespace Drupal\rest_entity_recursive\Normalizer;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\TypedData\TypedDataInternalPropertiesHelper;
use Drupal\serialization\Normalizer\NormalizerBase;

/**
 * Converts the Drupal entity object structure to a JSON array structure.
 */
class ContentEntityNormalizer extends NormalizerBase {

  /**
   * The format that the Normalizer can handle.
   *
   * @var array
   */
  protected $format = ['json_recursive'];

  /**
   * Default max depth.
   *
   * @var int
   */
  protected $defaultMaxDepth = 10;

  /**
   * {@inheritdoc}
   */
  public function normalize(mixed $data, ?string $format = NULL, array $context = []): array|string|int|float|bool|\ArrayObject|null {
    // Add the current entity as a cacheable dependency.
    $this->addCacheableDependency($context, $data);

    // If it is root entity set current_depth and root_entity in context.
    if (!array_key_exists('current_depth', $context)) {
      $context['current_depth'] = 0;

      if (empty($context['max_depth'])) {
        $context['max_depth'] = $this->defaultMaxDepth;
      }
      // Get max_depth from request.
      if (!empty($context['request']) && !empty($context['request']->query)) {
        $requestMaxDepth = $context['request']->query->get('max_depth');

        // Set max_depth in context.
        if ($requestMaxDepth === "0") {
          $context['max_depth'] = 0;
        }
        elseif ((int) $requestMaxDepth > 0) {
          $context['max_depth'] = (int) $requestMaxDepth;
        }
      }
    }

    /** @var \Drupal\Core\Entity\ContentEntityInterface $data */
    $entity_type = $data->getEntityTypeId();
    $entity_bundle = $data->bundle();

    // Create an array of normalized fields.
    $normalized = [
      'entity_type' => [['value' => $entity_type]],
      'entity_bundle' => [['value' => $entity_bundle]],
    ];

    // Set root entity in context.
    if (empty($context['root_parent_entity'])) {
      $context['root_parent_entity'] = [
        'id' => $data->id(),
        'type' => $entity_type,
      ];
    }

    $field_items = TypedDataInternalPropertiesHelper::getNonInternalProperties($data->getTypedData());

    // Other normalizers can pass array of fields to exclude from processing.
    if (isset($context['settings'][$entity_type]['exclude_fields'])) {
      $excluded_fields = $context['settings'][$entity_type]['exclude_fields'];
      $field_items = array_diff_key($field_items, array_flip($excluded_fields));
    }
    foreach ($field_items as $field) {
      // Continue if the current user does not have access to view this field.
      if (!$field->access('view')) {
        continue;
      }
      $normalized[$field->getName()] = $this->serializer->normalize($field, $format, $context);
    }

    return $normalized;
  }

  /**
   * {@inheritdoc}
   */
  public function getSupportedTypes(?string $format): array {
    return [ContentEntityInterface::class => FALSE];
  }

}
