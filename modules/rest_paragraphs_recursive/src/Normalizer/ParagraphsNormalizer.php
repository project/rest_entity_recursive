<?php

namespace Drupal\rest_paragraphs_recursive\Normalizer;

use Drupal\paragraphs\ParagraphInterface;
use Drupal\rest_entity_recursive\Normalizer\ContentEntityNormalizer;

/**
 * Paragraphs normalizer for json_recursive format.
 */
class ParagraphsNormalizer extends ContentEntityNormalizer {

  /**
   * Array of supported paragraph types.
   *
   * @var array
   */
  protected $supportedParagraphTypes = [];

  /**
   * Array of excluded fields.
   *
   * @var array
   */
  protected $excludedFields = [
    'revision_id',
    'langcode',
    'type',
    'uid',
    'status',
    'created',
    'revision_uid',
    'default_langcode',
    'revision_translation_affected',
    'behavior_settings',
  ];

  /**
   * {@inheritdoc}
   */
  public function supportsNormalization($data, ?string $format = NULL, array $context = []): bool {
    if (parent::supportsNormalization($data, $format)) {
      if (empty($this->supportedParagraphTypes)) {
        return TRUE;
      }
      if (in_array($data->getType(), $this->supportedParagraphTypes)) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function normalize(mixed $data, ?string $format = NULL, array $context = []): array|string|int|float|bool|\ArrayObject|null {

    // Ask REST Entity Recursive to exclude certain fields.
    $context['settings'][$data->getEntityTypeId()]['exclude_fields'] = $this->excludedFields;
    return parent::normalize($data, $format, $context);
  }

  /**
   * {@inheritdoc}
   */
  public function getSupportedTypes(?string $format): array {
    return [ParagraphInterface::class => FALSE];
  }

}
