<?php

namespace Drupal\rest_media_recursive\Normalizer;

use Drupal\file\FileInterface;

/**
 * File normalizer for json_recursive format.
 *
 *  @package Drupal\rest_media_recursive\Normalizer
 */
class FileNormalizer extends MediaNormalizer {

  /**
   * Array of excluded fields.
   *
   * @var array
   */
  protected $excludedFields = [
    'langcode',
    'uid',
    'status',
    'created',
    'changed',
  ];

  /**
   * {@inheritdoc}
   */
  public function normalize(mixed $data, ?string $format = NULL, array $context = []): array|string|int|float|bool|\ArrayObject|null {
    // Add the current entity as a cacheable dependency to make Drupal flush
    // the cache when the media entity gets updated.
    $this->addCacheableDependency($context, $data);

    // Ask REST Entity Recursive to exclude certain fields.
    $context['settings'][$data->getEntityTypeId()]['exclude_fields'] = $this->excludedFields;
    $normalized_values = parent::normalize($data, $format, $context);
    return $normalized_values;
  }

  /**
   * {@inheritdoc}
   */
  public function getSupportedTypes(?string $format): array {
    return [FileInterface::class => FALSE];
  }

}
